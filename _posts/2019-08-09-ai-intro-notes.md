---
layout: post
title: "Notes from Winston's introductory Aritificial Intelligence Lecture"
date: 2019-08-08
---

> Note: I wrote these notes months ago. But this seems like the perfect way to start blogging about AI.

# Notes from the first lecture of Patrick Winston's MIT AI course

> Winston died this July, a few days after I watched this lecture. I have seen only 2 of his many lectures (there is one about support vector machines which turns up on the first page of google search for quite a few search strings. The other one I watched was about the MinMax adversarial game tree search algorithm. Winston explained the algorithm, and a technique called *Alpha Beta pruning*. Then he asked the class- "is Alpha Beta pruning a different algorithm than MinMax?". Then, pausing a bit said- "Let me see your heads roll from side to side." "That's right! the answer is *NO*" "Alpha beta pruning is an augmentation of minmax which gives the same result". I will never forget this fact now!

### What *is* it?
* Thinking, Perception and Action
* For analysing TPA, we need models.
* This extends the definition to 'Models Targeted At Thinking, Perception and Action'
* To model something, you must have suitable representations for it.
* Now- 'Representations that support Models targeted at Thinking, Perception and Action'

#### What is a 'representation'

[The farmer wolf goat cabbage problem](/images/farmergoat.jpg)

* Consider the 'Tiger, Goat, Hay, Farmer cross the river problem'
* (Tiger eats goat and goat eats grain if left alone, and we want to make them all cross the river)
* To solve the problem, we need to represent it. This can be done in the form of a picture- with a river and two sides, which shows where which piece is.
* Now there are 2^4 possible states. (Each piece can be on either side). Of these, some are *loss* states (someone eats someone/thing)
* Two representations can be *connected* if there is a valid move from one representation to another.
* Now we have a model of the system in the form of a graph of valid states! And we can solve the puzzle!

* While exploring the system using the picture representation, we saw that there are certain *constraints* on the system.
* Patrick now makes the definition 'Constraints exposed by Representations that support ..'
* But that gives us nothing. What we want to do is solve problems using algorithms:
* The final definition is:

> 'Algorithms enabled by constraints exposed by representations that support models targeted at thinking, perception and action'

Which is now completely ununderstandable.

### Generate and Test:
* To find solution, generate possible solutions, test them.
* The solutions which succeed are the good ones, which can be used.	
* We do this all the time. Brute Force!

> When we give a name to something, we gain power over it. This is the Rumpelstinsken principle. --Patrick Winston

* The principal is not 'trivial'. Trivial ideas seem useless. 
* Simple ideas can be *powerful*. Things do not need to be difficult to understand to be useful.

* When we represent something well, it becomes easy to solve. When we draw the graph of the farmer problem, we can immediately see that there are only two solutions to it, by looking at the state diagram.

* AI is not *just* about thinking. It also requires perception.
* Q.: How many countries in Africa does the equator pass through?
* (Given map of africa with equator)
* The question is interpreted by our *language system*, which passes a proper representation to the *vision system*, which counts the countries and returns the number 6!

### History:

[Alan Turing](/images/turing.jpg)

1. Ada Lovelace- *The analytical engine only does what we tell it to, it never orignates something*
2. 1950: Turing describes the *turing test* for artificial intelligence
3. 1960: Marvin Minsky *Steps to AI*, Symbolic Integration!

This was the *dawn age* of AI, the age of speculations.

[Deep Blue](/images/deepblue.jpg)

4. Dawn Age: Programs that gave entertaining, nonsensical answers like *ELIZA*, programs which solved IQ test *analogy* problems were created. 
5. Late Dawn Age: Programs that learnt that flat objects balance on pillars, or *expert systems* that can diagnose disease, recommend medications. Expert systems that found efficient ways to land airplanes, save fuel costs.
6. Bulldozer Age: Deep Blue (in restricted domains, raw computational power = intelligence)
7. The Right Path: A loop of thinking, perception and action is involved.
