---
layout: post
title: "Fun with Data and Seaborn"
date: 2019-08-08
---

# Fun with Data and Seaborn

### Our college made data about various exam scores of students available. I was supposed to learn Seaborn as an assignment
### So here I am

I cleaned up the data so that all points have valid entries for all the attributes I have used in the plots.

I have extensively referred to https://www.marsja.se/python-data-visualization-techniques-you-should-learn-seaborn/

And stackoverflow.


```python
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np

import warnings
warnings.filterwarnings('ignore')

df = pd.read_excel('comp_data_clean.xlsx')
```


```python
# Convert CBSE and ICSE GPAs to percentage for SSC and HSC
def convertcg(x):
    x = float(x)
    if x <= 1:
        x *= 100
    elif x <= 10:
        x *= 10
    return x

df['HSC'] = df['HSC'].apply(convertcg)
df['SSC'] = df['SSC'].apply(convertcg)
```

A number of plots need data to be divided into discrete categories. *So I create them*


```python
# Create a new field which represents range of CET Score
def getrange(x):
    if x < 120:
        return "< 120"
    elif x < 140:
        return "120 - 140"
    elif x < 160:
        return "140 - 160"
    elif x < 180:
        return "160 - 180"
    else:
        return "180 - 200"
    
df["CET Range"] = df["CET Score"].apply(getrange)

```

# Scatter Plots

Scatter Plots are used to determine how much one variable is affected by the other

If all points on the plot lie close to a line, it indicates a high degree of corellation

A line called as the "Regression Line" can be calculated, which provides the best linear approximation of corellation

The shaded area is a confidence interval for the slope of the regression line.

### Plot of Entrance Exam Score vs University GPA
Some points lie *VERY* close to the deviation line. However, there are also a number of outliers on both sides. (Some people like engineering, while others prepared well for the entrance?)


```python
# How much does GPA depend on Entrance Exam Score?
cet = sns.regplot(x = "CET Score", y = "CGPA", data = df)
```


![png](output_7_0.png)


### Here is a plot of college GPA vs High School percentage
Less points lie *that* close to the regression line. However, the "scattering" is more, indicating less correlation


```python
# Scatter plot of CGPA with HSC Marks, without confidence interval
hsc = sns.regplot(x = "HSC", y = "CGPA", ci = False, data = df)
hsc.set(xlabel = "HSC Percentage")
```




    [Text(0.5, 0, 'HSC Percentage')]




![png](/images/output_9_1.png)


### And finally, a plot of GPA vs University Marks
While not dense near the regression line, I do see a trend here.. I suppose hard work (and rote learning pays off when seeking marks)


```python
# Scatter plot of CGPA with SSC Marks, with confidence interval
ssc = sns.regplot(x = "SSC", y = "CGPA", data = df)
```


![png](/images/output_11_0.png)


# Histogram
Simply used to visualize what amount of people lie in what range of GPA

In *seaborn* you can specify the number of *bins*, i.e the number of ranges you want to divide the points into


```python
# Distrubution of CGPA
histrogram = sns.distplot(df.CGPA, bins = 20, kde = False)
```


![png](/images/output_13_0.png)


## Grouped Histogram
Here is a distribution of GPA grouped by entrance exam score


```python
for cond in df["CET Range"].unique():
    cond_data = df[(df["CET Range"] == cond)]
    ax = sns.distplot(cond_data.CGPA, kde = False)
```


![png](/images/output_15_0.png)


# Bar Plots
These can be used for comparing properties of different **discrete** categories of data.

(I cheated a bit here by creating discrete categories from the Entrance Exam marks property, but now that I have a new property, I can use it for trying lots of interesting types of plots)


```python
# This cell makes no sense, as Y axis clearly shows *count*.
# However, the tutorial I am referring to does the same, and this is an assignment, so here I am
df_grpby = df.groupby("CET Range").count().reset_index()

group_plot = sns.barplot(x = "CET Range", y = "CGPA", data = df_grpby)
```


![png](/images/output_17_0.png)


#### Better
Now the labels make sense


```python
group_plot = sns.barplot(x = "CET Range", y = "CGPA", data = df_grpby)
group_plot.set(xlabel = "CET Range", ylabel = "Number of Students")
```




    [Text(0, 0.5, 'Number of Students'), Text(0.5, 0, 'CET Range')]




![png](/images/output_19_1.png)


# Box Plots
These are a better way of visualizing and comparing data according to discrete categories

Box plots show:
1. Meidan: The middle line in the box
2. Maximum: The upper bar
3. Minimum: The lower bar
4. First Quartile: Lower lone of the box
5. Fourth Quartile: Upper line of the Box

### Here is a boxplot of GPA of students grouped based on entrance exam marks


```python
fig = plt.figure(figsize = (7, 5))
box_plot = sns.boxplot(x = "CET Range", y = 'CGPA', data = df)
```


![png](/images/output_22_0.png)


# Heatmaps
These allow visualization of several variables together (multiple dimensions in 2D!)

### The following heatmap shows 4 different scores in the same diagram.

Data anonymized. Only numbers no names.

I suppose heatmaps are a bad idea when the properties are from different ranges. There are only so many shades of black a man can distinguish between..


```python
fig = plt.figure(figsize = (10, 7.5))
hmp = sns.heatmap(df[["CGPA", "CET Score", "HSC", "SSC"]])
```


![png](/images/output_25_0.png)


# Correlograms
The pandas *corr* method creates a matrix that shows the extent of corellation between variables.

The correlation matrix is symmetric, so half of it is masked off, and the remaining can be plotted as a heatmap in full glory.

## Interesting Correlations

Here I examine how different exam scores are correlated to one another.

Note that I did not say *affect*. 

> Correlation does not imply causation

Still, you do notice that SEM3 (which has the heavy and glorious data structures and algorithms course) is more associated with the overall GPA than the other semesters (nudge nudge wink wink)

Also, notice that the School, High School and Entrance Exam scores have < 0.5 correlation with GPA. I do not know the theory, but I suppose that's like tossing a coin..


```python
marks_corr = df[["CET Score", "HSC", "SSC", "CGPA", "SEM1", "SEM2", "SEM3", "SEM4"]].corr()

mask = np.zeros_like(marks_corr, dtype = np.bool)
mask[np.triu_indices_from(mask)] = True

fig = plt.figure(figsize=(12, 7))
corr = sns.heatmap(marks_corr, mask=mask, square = True)

# A sticky issue is that matplotlib breaks seaborn, causing the lowermost row of a correlogram getting halved
# This is very irritating using an otherwise perfect plot
# Based on https://stackoverflow.com/questions/56942670/matplotlib-seaborn-first-and-last-row-cut-in-half-of-heatmap-plot
# Can be fixed with manual intervention
corr.get_ylim()
corr.set_ylim(8.0, 0.0)
```




    (8.0, 0.0)




![png](/images/output_28_1.png)


# Violin Plots
These, along with the range (black bar) and the mean (white dot) show the *Probability Density Function* for the data!

Besides, they look beautiful.


```python
fig = plt.figure(figsize = (15, 7.5))
catviolin = sns.violinplot(x = "CET Range", y = "CGPA", data = df)
```


![png](/images/output_30_0.png)


# Raincloud Plots
These show the *violin plot*, *box plot* **and** *scatter plot*!

Seaborn does not provide RainClouds. So one must get them from the Ptit Prince (just a little lousy precipitation joke for you)


```python
import ptitprince as pt

fig = plt.figure(figsize = (20, 8))
ax = pt.RainCloud(x = "CET Range", y = "CGPA", data = df, orient = "h")
```


![png](/images/output_32_0.png)

