---
layout: page
title: About
permalink: /about/
---

I am a Computer Engineering student who is excited about learning classical AI (think LISP!), and resigned to the fact that I will have to eventually learn Machine Learning.

DISCLAMER: This is a course blog. However, I *will* try to keep it interesting.
